---
title: Modern Marketing's Role in Scaling the Vision for A New Community
categories:
    - company
author_staff_member: 01_jay
show_comments: false
---
![By Scott Saghirian - Flickr: Columbia Lake Front, CC BY 2.0, https://commons.wikimedia.org/w/index.php?curid=30743650](/images/blog/modern-marketing-role-in-scaling-the-vision-for-a-new-community/columbia_lake_front_large.jpg)

New cities start with a vision. It’s usually not the vision of a committee or a focus group or a government agency. New cities start with the vision of a founder. Even if the new city is being driven by a development company, often there’s a single company leader who carries and champions the vision.

This was just as true for the “new town” that I grew up in - [Columbia, Maryland](https://en.wikipedia.org/wiki/Columbia,_Maryland) - as it is for the new cities in Africa that DoubleGDP has been working with. Columbia was founded by the real estate developer and urban planner James Rouse.

If you read [his biography](https://www.amazon.com/Better-Places-Lives-Biography-James/dp/0874203422), you hear of stories of Rouse driving a golf cart around the undeveloped farmland, pointing here and there to show every curious new investor and many of the first residents exactly how Columbia would develop. Over here is where families will, in just a few short years, be shopping in the world-class mall. Over there, the children will be running through the parks. Just around the corner, the community will be gathering for concerts and parades.

Vision attracts the pioneering residents and businesses who one day will inhabit the new city. For them, the founder’s vision solves a problem, fits a need, fills a void. These prospects often believe more in the founder and the vision than in the physical evidence of construction happening to build the new city.

But here’s the problem: founders need to influence many thousands of people, over time, so that they see the vision, understand and connect with the vision, and act on the vision.

That’s hard to do as a single person. As a founder, sharing your vision one-on-one or with a small group can be emotionally gratifying. It may lead to prospective residents becoming champions and evangelists. But it doesn’t scale to the thousands of people a founder needs to influence over time to turn a vision and a plan into a thriving community.

How do founders scale themselves to share their vision widely, effectively, and consistently, and effectively?

&nbsp;  

# Scaling the Vision of a New City

In scaling how they share their vision, founders need to:

1. Reach large numbers of people both directly and indirectly.
2. Have people buy into them and their vision, before they buy into the community.
3. Stay in touch with people, building community and nurturing connections.
4. Convert connections with people into commitments to move into homes or office buildings.
5. Measure the growth and effectiveness in communicating the vision.

&nbsp;  

Marketers would say that this is a promotion problem. That might sound like a funny notion: promoting a city that doesn’t yet exist. Yet marketers promote the intangible all the time, whether it’s new products that aren’t yet available or social causes like justice and community-building.

![Gail Holliday's Columbia Posters](/images/blog/modern-marketing-role-in-scaling-the-vision-for-a-new-community/columbia-gail-holliday-posters.jpg)

When Columbia was being built, James Rouse and his team used the marketing tools of the day--like newspaper advertisements and billboards--to help attract new residents. I have a few colorful lithographs by the artist Gail Halloway that once hung in Columbia’s Exhibit Center to tell the story of this new town to people who wanted to learn more. They’re beautiful--and they’re exactly how you would use marketing to help a new town grow in the 1960’s.

Today, a lot of promotion in modern marketing happens through digital tools. Unfortunately, there’s not a single digital tool or platform that meets all the needs a founder has in scaling up how they share their vision. Founders aren’t alone in this; most marketing organizations and efforts today must assemble a suite of best-in-breed tools to carry out their promotional work.

Organizations refer to this suite of modern marketing tools as their marketing technology stack--their martech stack. (Feed that term into a search engine to get a sense of how these ecosystems function.)




&nbsp;  

# Tools for Scaling

Here’s a list of modern digital marketing tools that founders can assemble to help them scale the sharing of their vision:


&nbsp;  

| Founder's Need | Modern Marketing Software Tools | Examples |
| --- | --- | --- |
| *Announce the Vision* <br /><br /> | Audience Capture Applications | Launchrock <br /> Kickstarter<br />|
| *Build Out the Story* <br /><br /> | Content Management System (CMS) | Wordpress <br /> Joomla <br /> YouTube <br /> |
| *Keep in Touch* <br /> *Share Progress* <br /> *Nurture Interest over Time* <br /><br /> | Marketing Automation | Pardot <br /> Marketo <br /> Hubspot <br /> MailChimp <br /> |
| *Create Community* <br /><br /> | Social Networks <br /> Event Marketing <br /> | Facebook <br /> Instagram <br /> Nextdoor <br /> EventBrite <br /> Zoom <br /> WebEx <br /> |
| *Transform Contacts into Commitments to Build Homes* <br /><br /> | Contact Management System | Salesforce <br /> Hubspot <br /> Odoo <br /> |
| *Measure Growth* <br /><br /> | Analytics | Google Analytics <br /> Qualtrics <br /> |

&nbsp;  

# Lessons in Scaling a City Vision

Through our work in Africa at DoubleGDP, we’ve learned two key insights about the founders of new cities, their audiences, and promoting a vision.

**Show progress.** The residents of new towns in developing countries often invest and build “one block at a time.” Which, of course, is the same way that the new community will develop. To accelerate people and business making commitments and moving into the community, the most important thing that a founder can do is to show proactive, regular demonstrations of progress.

Conducting these types of demonstrations at scale is logistically difficult and time consuming if you don’t have a system to publish and distribute progress updates. With digital marketing, you can publish updates to your WordPress website and your YouTube page, then share and promote those updates through various channels such as Facebook, Instagram, NextDoor, WebEx, and MailChimp.

**Model consistency.** Prospective residents and businesses in developing countries don’t always experience consistency in their civic environments. For new cities, the way that founders share the master plan, the way that the development companies explain rules, and the way that they consistently communicate and stay-in-touch is one of the most important early indicators of trust and believability. Consistent communications reinforces the sense of order and stability that is often a major need that residents and businesses are seeking to meet.  

&nbsp;  
&nbsp;  

As a single founder--even one with a great and dedicated team--maintaining consistent contact with thousands of people is difficult to do solely through personal communications. Using modern marketing tools to regularly publish updates, information, and insights for your audience makes consistency easier to achieve and maintain.


*On March 17-18, 2020, we will be joining a group of visionary founders of new cities at the Charter Cities Conference in Johannesburg, South Africa. Please reach out if you’re going to be at the conference. We’d love to hear your vision for cities of the future, how you’re trying to bring that vision to life, and what we might contribute.*

*Email me at [**jay@doublegdp.com**](mailto:jay@doublegdp.com) and we’ll set some time up to talk.*
