---
title: An Operating System to Create City Operating Systems
categories:
  - company
author_staff_member: 01_jay
show_comments: false
---

## How we’re taking a “replicate then innovate” approach to getting our organization’s purpose, values and working culture right from the start.

DoubleGDP is a technology startup that’s building new software for new cities.

At the start of a new business, there’s a lot to think about: What can we offer that’s special and different? What challenges are worthy of pursuing with the limited time and resources we have?

Above all, the two most important questions are maybe the simplest to ask.

<strong>Why?</strong> And, <strong>How?</strong>

Here’s how our team at DoubleGDP is beginning to approach and answer those questions.

Why support the development of new cities? Because we believe the world needs more [great places to live](https://doublegdp.com/doublegdp/2019/08/30/why-doublegdp/).

Where people live impacts growth in all sorts of ways - “the growth of individuals, the growth of families, the growth of communities, the growth of economies.” We covered this recently in more detail, so won’t elaborate here.

Some environments provide momentum and opportunities - others present headwinds like a lack of affordable housing, health care and education. Especially in emerging markets, right now there’s not enough urban infrastructure to support the pace at which populations are growing.

The “why” for this company has been clear since the get go.

But how should we get there? What type of a company do we want to have? What types of values do we care most about when we work together? How can we bring those values to life everyday while we’re designing, coding, facilitating, meeting, discussing, debating? How can our values shine through in how our customers and stakeholders experience our products?


## The “Operating System” of a New Software Company

The purpose, values, culture, habits and norms of an organization can be thought of as the organization’s operating system (OS). They set the underlying “code” by which collaboration happens, decisions get made, problems get solved and progress occurs. An organization’s operating system is often invisible, but it influences everything.

If there’s anything that each of our earliest team members’ previous businesses have in common, it’s a careful focus on values and culture at the core of their organizational designs.

In fact, it was a visit to observe the culture and operations at GitLab that first [brought several of us together](https://www.linkedin.com/pulse/high-efficiency-innovation-three-lessons-learn-from-gitlabs-newman) to talk about how to create software organizations that deliver innovation through rapid execution - and keep scaling.

We will need to be very intentional about creating this OS for DoubleGDP the company. Because it will directly impact the type of software we build, the way our customers and their residents experience our platform, and the impact that it might have on the character and effectiveness of new cities.

What type of software company do these new cities need us to be?


## Designing Our Approach to Building from Best Practices

One of our closest partners is the [Charter Cities Institute](https://www.chartercitiesinstitute.org/). The Institute is a fountain of knowledge about new cities both now and throughout history. And it’s the leading think tank in Washington DC focused on a particular type of new city: charter cities.

A [charter city](https://www.chartercitiesinstitute.org/intro) is a city given a special jurisdiction to create a new governance system.

One of the critical insights the Institute has taught us is that new cities and urban developments have an important advantage - from the moment of their foundation onward, they can immediately leverage the best practices, conventions and efficiencies (aka [governance](https://web.stanford.edu/~chadj/RomerNobel.pdf)) that many successful cities and governments have learned over time.

So when working with new cities, one central goal of our software is to help the property developers and community managers of these cities take advantage of these best practices. So they can be most efficient in supporting the kind of lives and growth their residents need.

Imagine: a young entrepreneur can quickly get licensed to start their new business. An aspiring citizen can run for public office secure that process will be fair and corruption kept in check. A homeowner can hold city services accountable when her road is impassable and keeping her children from school.

Our software needs to help make best practices like these efficiently replicable. And our company should do the exact same thing.

So instead of starting from scratch, with regards to culture, values and ways of working, we’re going to replicate before we innovate.


## Replicate, then Innovate

GitLab has designed the best culture we know of - as of now - for creating a software company that builds end-to-end digital platforms at the scale we plan for.

And one of the ways they’ve done it has been by encoding all the parts of the culture - it’s values and habits and standards of operation - into its [handbook](https://about.gitlab.com/handbook/). It’s the operating system for a new type of software company. And they openly share it with the public, so everyone can contribute.

We’re copying the [core values](https://about.gitlab.com/handbook/values/) from GitLab over for DoubleGDP. We’re going to start with Collaboration, Results, Efficiency, Diversity, Iteration and Transparency as the core operating values of our company.

Here’s a link to a [document](https://doublegdp.com/progress/04-handbook/) where over the coming weeks, our team is going to build off this clear starting point and define the rest of an operating system that’s tuned to exactly what we’re creating.

There will certainly be some differences in our cultures. Among other things we know that DoubleGDP’s business needs to have a strong orientation towards multiple stakeholders. We’re not just designing DevOps for engineering teams - our software should understand the needs of residents, real estate builders, construction teams, architects, planners, community organizers, government leaders and international development agencies.

Starting from the best tech culture we know is a great first step to setting the direction of DoubleGDP.

But it’s also more than that - it’s a way for us to practice what we preach.
