---
title: Product Update March 2022
categories:
    - company
    - product
author_staff_member: 2022-02_craig
show_comments: false
---


Time really does fly! We are all guilty of uttering this phrase from time-to-time, but its meaning has never been more clear to me than now.  As I near the end of my first month as Head of Product here at DoubleGDP, I thought I would take an opportunity to provide you with some updates on the “what” and “why'' of some of our recent product improvements and outline our areas of strategic focus as we set our sights on Q2 2022 and beyond.

<img src="/images/blog/2022-03-29-product-update.png" width="150" />

## New Feature - Digital First Process & Task Management

What: Our new process management module enables you to streamline the various tasks, documents, and collaborators that power your design, planning, construction and home handover workflows.

Why: Building the cities of the future requires more than just ticking boxes on a paper checklist. Administrators, architects, engineers, city planners, contractors and developers must operate with great precision to complete the tasks and processes necessary to deliver projects on time and within budget and enable satisfied residents to move into their new property. Even one small change in plan may involve many different parties reviewing, revising and approving so building progress is not hindered. Often they are on the move and need to be able to view and collaborate seamlessly on a mobile device.

What’s Next: As we expand this feature, we plan to enhance task views so users can more easily see tasks that require their attention and enhance collaboration by allowing comments linked to a specific point of interest on a document. While we have initially focused on construction and handover processes, we plan to expand to include other common workflow and custom templates.

## New Feature - Increase Sales with Lead Management

What: Purpose-built CRM functionalities allow you to capture, track, manage and convert prospective residents in one integrated system including email campaigns, lead pipeline statuses,  commenting, and integrated task and process management. 


Why: From viewing floor plans online, to visiting the city in-person to select a plot, there are many different paths prospective residents can take before deciding to call your community home. Email marketing and CRM solutions abound, but no one else can provide integrated functionality to track all of your sales and marketing engagement touchpoints from a lead’s first visit through your gate or first inquiry on your website all the way to receiving the keys to their new property.

What’s Next: Integrated plot management will allow you to assign and track the building lots leads are interested in. Enhancements to campaigns will enable further automation of followup messages and tasks.

## Coming Soon - Private Community Hub

What: Central feed of community news, events and commentary to better connect with current and prospective residents.

Why: Vibrant communities sell themselves. Your best sales and marketing tool is a community of connected residents. Our new community hub will help you stay connected with the happenings of your city, share news and updates as you grow, market community events, and ensure resident satisfaction is always top of mind.

Looking forward to sharing more of what we have accomplished next month.
