---
title: City COVID Responses
categories:
    - company
author_staff_member: 2019-10_nolan
show_comments: false
---

![SF COVID Order](/images/blog/2020-04-06-covid/2020-04-06_20-38-18.png )

## Sheltering In Place In San Francisco and Lusaka

As I’m watching how the world responds to the COVID-19 pandemic, I can’t help but wonder what is a city’s responsibility in addressing a major crisis like this.

I made my first tweet to an elected official, San Francisco’s mayor London Breed, to thank her for issuing a “shelter in place” order on March 16. She was the first city leader in the US to make this decision, and her early action has helped keep San Francisco’s rate of infection much lower than comparable US cities.

Because of the mayor’s order, I’ve had to update my home work environment. I set up a desk and bought a new web camera for my computer. My girlfriend and I are a bit cramped as we both work from home in our 1-bedroom apartment. But much of the DoubleGDP team resides in Lusaka, Zambia, and the challenges for them to shelter at home have been greater.

Although Zambia’s progression with COVID-19 trails other countries, we wanted to be proactive in preparing employees for potential quarantine orders. We helped our employees set up home offices with solar power and batteries. We provided mobile hotspots to keep them connected to the internet. Add in a desk and chair, and they were ready to work.

It’s much more difficult for most residents of Lusaka. Homes are often shared among many family members, friends, and roommates. Many residents carry water from a shared community pump instead of having running water. The 12-14 hour per day electrical blackouts can’t be overcome so easily for those without a US-based company to provide batteries. And more, much of the country’s economy is informal and can’t be sustained online at home. A shelter-in-place order could make it hard for many households to put food on the table — or even to access water.

In both San Francisco and the US, Lusaka and Zambia, there’s ample room for improved relationships and communications between cities and their citizens. SF’s Mayor Breed [updates Twitter regularly](https://twitter.com/LondonBreed?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor) and there’s lots of media outlets to explain what’s going on. But the city’s [formal order](https://www.sfdph.org/dph/alerts/files/HealthOfficerOrder-C19-07b-ShelterInPlace-03312020.pdf), is 20 pages of legalese. In Zambia, the Ministry of Health livestreams announcements and updates on [Facebook ](https://www.facebook.com/mohzambia)and [Twitter](https://twitter.com/mohzambia). Social media and news outlets are better than nothing, but in this day of personalized communications there must be something better.

**If every company I’ve ever done business with [is updating me on their coronavirus response](https://www.washingtonpost.com/opinions/2020/03/23/yes-this-business-still-has-your-email-heres-how-were-responding-covid-19/) why can't my city?**

Shouldn’t I be able to hear directly from my city? Get regular updates? Be able to respond and let them know about how it’s affecting me, or things they can do better? Although we have not seen a pandemic like this in decades, I know that crises like this are not new, and the world will have many more to grapple with.

At DoubleGDP, we’re seeing that cities need a management structure and platform to support delivering services and distributing aid to those affected by any large-scale event, not just a pandemic. Our teams in San Francisco and Lusaka aim to make city-citizen teamwork in times of crises easier and smoother moving forward.
