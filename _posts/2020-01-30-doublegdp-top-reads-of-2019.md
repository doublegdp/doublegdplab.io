---
title: DoubleGDP's Top Reads of 2019
categories:
    - company
author_staff_member: 01_jay
show_comments: false
---
![Reading by Bridge](https://www.doublegdp.com/images/blog/landscape-water-book-read-person-winter-618075-pxhere-com.jpg)

Every two weeks, our close partners at the [Charter Cities Institute](https://www.chartercitiesinstitute.org/) send out a great [email newsletter](https://chartercitiesinstitute.us12.list-manage.com/subscribe?u=207b1e68b981c1c089acb4bae&id=fccc97d8cc) capturing insights about “What They’re Reading” and what’s new in the fast-growing market for new towns and charter cities for which DoubleGDP provides a technology platform. 

I’m always inspired by what they’re reading and sharing. Every step of the way while we have been building DoubleGDP over the last year, we’ve similarly been sharing our own observations to our broader community through the [“What We Are Learning”](https://www.doublegdp.com/progress/05-what-we-are-learning/) page on our website. 

The beginning of a new year offers a great opportunity to look back and reflect on where one’s been. I wanted to take a few minutes last week to re-read our top observations, insights and learnings from 2019. 

This was the year that everything started for DoubleGDP.

The following pieces--articles, books, reports, podcasts--gave new shape and depth to our work with new towns and charter cities over the course of the year. Specifically, they: 

1. Helped us learn more about what our **first customers and partners** care about and need.
2. Deepened our understanding of the **market opportunity for new cities** so that we could align our vision for tech with potential for impact.
3. Provided insight into the progress of new cities in **emerging markets and Africa**, where we’re starting to build our business.
4. Gave context to the innovations of other **technologies for governments and smart cities** and how they might apply to our market.
5. Helped us make this business **personal and meaningful** to us, the members of the DoubleGDP team.

My hope is that something here in our reading list resonates with you as well. 

If you’re planning on attending the upcoming [Charter Cities Conference](https://chartercitiesconference.com/index.php) on March 17-18 in Johannesburg, let’s meet up and talk about what moved your own thinking and efforts forward over the last year.

&nbsp;  
&nbsp;  

![Building Nkwashi](https://www.doublegdp.com/images/blog/doublegdp-top-reads-of-2019/nkwashi-picnic.jpg)
# Building Africa’s Future
The Neoliberal Podcast sits down with Mwiya Musokotwane, an entrepreneur working to build new cities from the ground up in Africa to help meet Africa's need for new urban housing and high quality jobs. Mwiya co-founded Thebe and has been responsible for taking the Nkwashi project from being just an idea through becoming a new city with its first residents moving in. 

[Listen to the podcast.](https://podcasts.apple.com/us/podcast/the-neoliberal-podcast/id1390384827?i=1000434691265)

&nbsp;  

![Africa By The Numbers](https://www.doublegdp.com/images/blog/doublegdp-top-reads-of-2019/Africa-By-The-Numbers.jpg)

# The Race for New African Cities, By the Numbers
Of the 150-200 new city projects currently underway, many are under development in Africa. That’s in part because the continent’s population will grow by another 1.3 billion people by 2050. That’s like everyone in China moving to Africa over the next thirty years. By then, Nigeria will be one of the three most populous countries in the world. How is the continent, and the rest of the world, preparing for the ascendent Africa? Eighteen African new city projects are profiled here in this report.

[Read the Quartz article](https://qz.com/africa/1461626/eko-atlantic-diomniadio-tatu-africas-new-billion-dollar-cities/)

&nbsp;  

![Jazz City](https://www.doublegdp.com/images/blog/doublegdp-top-reads-of-2019/jazz-city.jpg)

# A Maker City is a Jazz City
Twentieth-century cities grew from the top down in a choreography of development. Peter Hirschberg argues that the new, “smart” cities are growing from the bottom up through the improvisational jazz of participation and experimentation.

[See the video and read the presentation recap.](https://techonomy.com/conf/detroit-15/perspectives/a-maker-city-is-a-jazz-city/)

&nbsp;  

![Technology in Emerging Markets](https://www.doublegdp.com/images/blog/doublegdp-top-reads-of-2019/emerging-markets-tech.jpg)
# Technology in Emerging Markets
Professor John Macomber, Senior Lecturer in Business Administration at Harvard Business School, has a bone to pick with the term “emerging markets.” Such a blanket description hides an incredibly diverse set of business and social ecosystems, each with its own set of obstacles, and necessities. Whether in Jakarta or Dubai, understanding the context is key. 

[Read the interview.](https://digital.hbs.edu/digital-infrastructure/technology-in-emerging-markets-an-interview-with-john-macomber/)

&nbsp;  

![City O/S](https://www.doublegdp.com/images/blog/doublegdp-top-reads-of-2019/City-OS-1040.png)
# A New City O/S: The Power of Open, Collaborative, and Distributed Governance
Based on their decades of direct experience and years studying successful models around the world, former mayor and Harvard professor Stephen Goldsmith and New York University professor Neil Kleiman propose a new operating system (O/S) for cities. Their system builds on the giant leaps that have been made in technology, social engagement, and big data. This is a great, practical resource for practitioners, starting with public-sector executives, managers, and frontline workers.

[Learn more about and purchase the book.](https://www.brookings.edu/book/a-new-city-os/) 

&nbsp;  

![Burning Man](https://www.doublegdp.com/images/blog/doublegdp-top-reads-of-2019/Burning-Man.jpg)
# A Noble-Winning Economist Goes to Burning Man
Paul Romer is former chief economist for the World Bank. He’s also the economist who most envisioned the first general theories of charter cities. In this New York Times article, he visits the world’s most famous pop-up city and muses about how different approaches to governance help communities work.

[Read the article.](https://www.nytimes.com/2019/09/05/upshot/paul-romer-burning-man-nobel-economist.html)

&nbsp;  

![Columbia, MD](https://www.doublegdp.com/images/blog/doublegdp-top-reads-of-2019/Columbia-MD.jpg)
# It Can Happen Here: A Paper on Metropolitan Growth
I grew up in the “new town” of Columbia, MD that James Rouse founded in the 1960’s. This 1963 speech became the basis for Rouse’s social theories on new towns. Personally, I wanted to re-read this while we were starting DoubleGDP. As an entrepreneur and designer, work is personal. I think it’s so important to really connect with why the mission of your work is deeply meaningful for you as an individual. And I see so many similarities between the vision that Rouse set for “better places, better lives” in designing his new towns, and the visionary founders of the new cities in Africa that we are so lucky to get to work alongside. 

[Read the report.](https://d2814gcejiq38s.cloudfront.net/wp-content/uploads/2016/06/RGI-S5_1-b4-f4ItCanHappenHere1963-09-26.pdf) 

