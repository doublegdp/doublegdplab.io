---
title: Towards a New City, Block by Block
categories:
    - company
author_staff_member: 01_jay
show_comments: false
image: /images/blog/2020-05-28-blocks/blocks-4133619_1280.jpg
---

![One resident’s query about cement blocks got me thinking about building faith and loyalty in new cities.](/images/blog/2020-05-28-blocks/blocks-4133619_1280.jpg )

Building a new city requires taking a million small steps towards a big vision of the future. Founders of new cities often promote the vision and value of the new city at the end of that journey. DoubleGDP’s role is to help these founders [make their vision a reality](https://www.doublegdp.com/company/2020/02/28/modern-marketing-and-visions-for-new-cities/). So, what have we learned about the best ways to keep people engaged and moving forward during a development process that might look to some like a million-step march? 

Communicating with residents (today’s and tomorrow’s) and investors is always important to maintaining engagement. We’ve seen time and again that for a new city or innovative mixed-use development, the pioneering residents and investors sign up because of the strength of the vision. We used to think that the best engagement came from showing how recent steps taken relate to the overall vision of a new city. 

We’ve now learned that there’s a risk to focusing too much marketing effort on showing how all the things add up to the whole. People “sign up” based on vision but they “move in” based on progress. Supporting progress requires a second, equally important but different approach to marketing and engagement. It’s about residents knowing you’re there and aware of the little, practical things that feel substantial to them at that moment.

For example, [Nkwashi](http://nkwashi.com/) is a new city near Lusaka, Zambia that uses the DoubleGDP platform. Recently, one of the residents of Nkwashi sent a message on the DoubleGDP platform, asking if the city had cement blocks. Her message didn’t have much detail, so we were puzzled. Why cement blocks? How many? What type?

We followed up with her and found the answer was simple: she needed a particular type and quantity of cement blocks to build her new house in Nkwashi. As our software team helps to enable a new city to be built, we can get fixated on looking up at the grand vision for the city and forget to look around to see if the people need help in taking the next right step. They need blocks to build with. 

And the next right step can be very mundane and practical. At the moment of the message, what this resident most needed was to take the next step on ordering cement blocks and having them placed on her plot. The next step could be as small as using the DoubleGDP platform to quickly get the right phone number for a local and approved contractor at Nkwashi. That could build a lot of loyalty and increase a resident’s faith this new city - by helping her feel like she's making progress toward her future life. She might even share her loyalty and faith with those around her. That’s how great vision becomes reality on the scale of a city.  

A consistent sense of forward progress is often more important than relating each little part to the bigger whole. And feelings of progress require a very different approach to marketing and engagement. Taking this lesson to heart, we recently implemented a software feature called “News Updates” on the DoubleGDP app, to show Nkwashi residents progress in and around their city. These updates will help Nkwashi and DoubleGDP to document and communicate the last few steps accomplished, the next few steps to take, and what steps residents can take now to help their city grow. 

In reality, the new software and content required for the first Nkwashi News Update was just five pictures on a website: the main gate, newly paved roads, the reservoir behind the dam full after the summer rains. Still, the response from owners and investors and prospects was huge. Which led to our second Nkwashi News Update: an interview with a local contractor who’s building homes today and who, in fact, can help you acquire blocks to build with.

Maybe it’s not a million-step journey to reach a new city. To borrow an image from the novelist E. L. Doctorow, maybe it’s more like driving at night in the fog. On such a trip, you can only ever see to the end of your headlights. Yet, no matter how long the drive, you eventually arrive home.

(Matthew Spaur contributed to this article. Learn more about Matthew at [Marketing the Social Good](https://www.marketingthesocialgood.com/).)
