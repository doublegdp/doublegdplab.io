---
title: Elevators and Traffic Lights
categories:
    - company
author_staff_member: 2019-10_nolan
show_comments: false
image: /images/blog/2020-04-28-elevator/elevator.png
---

![Elevator With Traffic Light](/images/blog/2020-04-28-elevator/elevator.png )

## Elevators and Traffic Lights: Designing Responsive City Services

Last week I spent nearly ten minutes waiting for my building's elevator. It gave me time to think about how cities can be more responsive in the face of change.

Because of social distancing rules required by the COVID-19 pandemic, my building's management has restricted use of an elevator car to just one household per trip. If my girlfriend and I board on the nineteenth floor and travel to the lobby, no one else can ride down with us. But the elevator doesn’t know this, and so it continues to stop and open on every floor while we smile awkwardly at neighbors who are not allowed to enter.

Each trip takes longer than it should and the elevator’s algorithm that starts at the highest floor and works its way down is no longer fair to lower floors. It would be much more efficient and fair to take requests in the order they were received, bring them directly to their destination, and then go serve the next person.


### Adding Digital Infrastructure to Physical Infrastructure

It would be ideal if our elevators were more easily configurable to adapt to new rules. It’s one example of how digital infrastructure (the elevator’s algorithm), if we had the right systems, could be tuned or changed to make better use of existing physical infrastructure (the elevator itself).

A city’s traffic lights present a similar opportunity.

Around the US,[ cities are closing streets to cars](https://www.nytimes.com/2020/04/11/us/coronavirus-street-closures.html) to give pedestrians more room to exercise close to home while obeying social distancing because of the pandemic. Here in California,[ San Francisco is closing 12 major streets](https://www.sfgate.com/bayarea/article/San-Francisco-is-closing-slow-streets-15215512.php) to cars. Oakland has closed 74 miles of streets.

How easy is it to change the traffic lights and signage to accommodate the new use of these streets? Researchers have developed algorithms to [optimise city-wide flows](https://www.govtech.com/fs/news/Traffic-lights-Theres-a-better-way.html), but as this [story from Charlotte, NC](https://www.wfae.org/post/faq-city-how-get-poorly-timed-traffic-light-fixed#stream/0) shows, most reporting emphasis is on getting feedback on specific intersections that seem broken. It’s great that they can hear from citizens, but probably won’t get the city the kind of macro view it needs. (After all, each person waiting for the elevator in the building may have a sense that like the system is broken, but from their local vantage point it mostly seems like bad luck -- “darn, the elevator opened _again_ with someone already in it!”)


### Building Street Smarts

San Francisco is currently in the middle of a[ multi-year project to reconfigure traffic control](https://www.sfweekly.com/news/city-plans-changing-hundreds-of-traffic-signals-by-2020-to-help-reduce-pedestrian-deaths/) across the city in an effort to reduce traffic deaths, especially for pedestrians. Some of this work requires physical construction or alteration. Other portions of the work involve reconfiguring traffic lights and signage.

A well-designed digital infrastructure driving the physical lights and signs could make them more adaptive and responsive, that same as a digital infrastructure controlling the physical infrastructure might for my building's elevators.

Such adaptive technologies would be helpful beyond the current pandemic. Routing traffic more easily in response to weather, accidents, and special events like concerts and sports can make a city work more efficiently. Across the Bay from me, a 20-mile stretch of Interstate 80 has been transformed into the[ area's first "smart" highway](https://www.sfchronicle.com/bayarea/article/Dark-Caltrans-signs-on-I-80-really-do-work-12605850.php) featuring several such adaptive technologies. That project has been years in the making.

For most cities the traffic grid has developed over decades of planning, growth, and evolution. Developing a digital infrastructure to run it efficiently and adaptably will require its own planning process and upgrades to physical infrastructure. It’s not necessarily an easy task. However, the new cities that DoubleGDP works with have the opportunity to build their software management approach with adaptability in mind from the beginning.

And that digital infrastructure and vision of responsive city services is something I can enjoy pondering while waiting for the elevator.
