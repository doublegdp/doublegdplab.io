---
title: Why DoubleGDP
categories:
  - company
show_comments: false
---

Where people live impacts growth.

By this, we mean growth in every form - the growth of individuals, the growth of families, the growth of communities, the growth of economies. We mean growth in terms of health, wealth, relationships, enterprise, knowledge and understanding.

We believe the world needs more great places to live.

It’s true everywhere, but especially true in emerging markets. Today, physical and digital infrastructure inhibit growth. As a result, there aren’t enough jobs, affordable housing, health care or education.

Right now, there are hundreds of new cities being planned and developed to create new opportunities for growth. In Africa alone, there are over [a billion square acres](https://estateintel.com/app/uploads/2018/10/New-cities-in-Africa-Report-2-compressed.pdf) of new cities and land reclamation efforts underway to meet the urban living needs of the growing population.

Some are following in the footsteps of communities like Singapore, Dubai and Shenzhen, adopting special economic charters to create new types of businesses and ways of working. Others are brand new neighborhoods and planned communities coming to life through mixed-used development.

People who are drawn to these new places come for a wide variety of reasons - better homes, better jobs, closer communities, better lives.

DoubleGDP builds new software for new cities.

We’re focused on building software for new cities because we believe that a city and its technology can and should be developed from the ground up, together.

We’re optimists about growth. We’re also practical and respect the limits of our role. It’s not our calling to create these new cities. Others are called to that, and are more suited to succeed. We work alongside the most amazing community builders and property developers around the world who are the right ones to build. They live the culture and keep local traditions in mind.

But we do believe that everyone can contribute.

Our role is to build software that helps new cities run better. To nail the digital infrastructure for these communities. To leverage best practices, conventions and efficiencies that are already available. And importantly, to listen. To help cities be open, transparent, responsive and accountable for the people who live there.

Support growth. The time is now and great opportunity is at hand. As much as double the world’s GDP could be at stake.

<strong> Build new cities. Nail the software from the ground, up. </strong>
