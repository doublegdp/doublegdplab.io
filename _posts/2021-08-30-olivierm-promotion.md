---
title: Promotion Spotlight - Olivier JM Maniraho to Senior Software Developer
categories:
    - company
author_staff_member: 2020-03_nicolas
show_comments: false
image: /images/blog/2021-08-29-olivier/olivier.jpg
---

![Undated photo of Olivier](/images/blog/2021-08-29-olivier/olivier.jpg)

DoubleGDP is proud to announce the promotion of Olivier JM Maniraho to Senior Software engineer.  Olivier joined DoubleGDP on October 19th, 2019 as our first Mid-level Software engineer. While originally a consultant with DoubleGDP, Olivier took on a full-time role with us 9 months later to be fully committed to the success of our product. Since his first day with DoubleGDP, Olivier has been dedicated to a high-quality work ethic. He has continuously applied his technical expertise, collaboration, and communication skills across several key projects and has been a critical member of the teams he has worked with at DoubleGDP.
Some of his accomplishments include:

 - Taking both development and leadership roles in changing our forms feature to include categories and contract capabilities.
 - Improving our production deployment processes.
 - And implementing a modular architecture in ReactJS for easier management.
His new responsibilities will now include running scrum meetings, interviewing and training new candidates, and ensuring coding standards are enforced.

*We recently sat down with Olivier to talk about his time at DoubleGDP, what he’s learned in his almost 2 years here, and what he’s looking forward to in his new role.*


**How did you come to work in software engineering?**

**Olivier:** I studied computer science at University but I initially had to drop out due to financial reasons.   After dropping out, I had a whole list of things that I personally needed to learn and decided to create my own program.  Within a year,  I had read and practiced all the exercises in “Practical Programming in Python”, a book my friend had given me.  

Later, I had an opportunity to work as an intern at BongoHive and then found a junior software developer position at HackersGuild.  I could finally afford to finish school and went on to complete my studies.  That’s how I started my software engineering journey.  

Since that time, I have worked with local and international organizations as well as NGOs.  And, I have spoken and helped organize conferences, and tech-focused meetups as well as taught at developer boot camps, mentored junior developers, volunteered in programs that focused on bringing technology to rural communities in different parts of the country.

**What have you learned during your time at DoubleGDP? What do you think has been your biggest accomplishment here so far?**

**Olivier:** Since starting at DoubleGDP close to 2 years ago,  I have learned the following:
- Asynchronous communication
- Open communication, all communications should be public - Transparency
- Responsibility - Owning the code you write
- Iteration - Find the simplest solution that solves the problem and iterate on it.
- Documentation of all processes

Personally, Working with highly motivated and skilled engineers from different parts of the world has been the most rewarding experience at DoubleGDP.  We are always ready to solve whatever technical problem comes our way TOGETHER.

And, seeing the company grow both in clients and the number of software engineers has been very rewarding.

**What do you hope to accomplish in your new role as a Senior Software Engineer?**

**Olivier:** I always do my best to do better than I did yesterday.  In the hopes that I can prepare for the next step in my career.  I hope to learn more and work even better with the engineering team to accomplish more and help others be productive in their daily work.
