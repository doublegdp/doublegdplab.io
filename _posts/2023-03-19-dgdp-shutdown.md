---
title: DoubleGDP Platform Shutdown Notice
categories:
    - company
author_staff_member: 2019-10_nolan
show_comments: false
---

DoubleGDP started in 2019 with a mission to help new cities connect with residents, accelerate growth, and deliver responsive public services. Over the last 3 years the new city market did not grow as quickly as we had expected and hoped, and we have thus decided to cease operations and shut down the company.

**April 28, 2023 will be the last day of service for the DoubleGDP platform.** After that day, you will no longer be able to log in to the system or retrieve exports of the data that it held. Please make arrangement prior to this day to find alternative solutions to any active processes, and ask for an export of any data that is critical. We have reached out individually to those who we think are most likely to want this service, but please email <a href="mailto:nolan@doublegdp.com">nolan@doublegdp.com</a> if you need help.

We have made our [source code available](https://gitlab.com/doublegdp/open-platform) via an open source [MIT license](https://opensource.org/license/mit/) and have compiled a list of [alternative platforms](https://docs.google.com/document/d/1F-0wvQBkuFc4oYqsIob5as_mV-1qqkYi_Es8CsTzrnY/edit) that can provide parts of what the DoubleGDP platform was doing. Take a look at those resources and/or reach out to us if you need alternative arragements.

From all of us at the DoubleGDP team, thank you very much for your usage of the platform and engagement with our team. We had such a wonderful time working with so many great people around the world. We are great believers in the potential of new cities and wish you the best on your endeavors. We wish we could continue the journey with you!

Nolan
