---
title: Product Update May 2022
categories:
    - company
    - product
author_staff_member: 2022-02_craig
show_comments: false
---


The arrival of spring and warmer weather for those of us in the northern hemisphere has coincided well with a recently announced expansion of our product focus. After two years learning and growing the DoubleGDP app alongside our partner cities, we have made the decision to expand our focus to the broader segment of planned communities of all shapes and sizes. As we began to consider this new type of community, we realized that many of them are just cities on a smaller scale with many of the same services that new cities provide. As you review our areas of product development below, you will see how this broadened focus has already begun to shape the features we are building.

## New Feature - Gate Access Kiosk Mode

What: Resident and visitor centric interface for a wall mounted smartphone or tablet to allow for self-scanning of QR access code to enter a community.

Why: Peak times of day and special events can create a backups of people waiting for access to communities. This new kiosk mode allows registered users to tap a few buttons and scan their QR code on a mounted display to augment the throughput of guards at the gate. This feature is great for expanding capacity during community events, peak hour entrance times, or communities where the guard gate may be physically distant from an access gate but can open it remotely.

## Releasing Soon - Private Community Hub

Our engineering team has been hard at work on our new community hub that I announced in my last update. Here is a recap of what the feature is all about and some screenshots of how it will look in the app.

What: Central feed of community news, events and commentary to better connect with current and prospective residents.

Why: Vibrant communities sell themselves. Your best sales and marketing tool is a community of connected residents. Our new community hub will help you stay connected with the happenings of your city, share news and updates as you grow, market community events, and ensure resident satisfaction is always top of mind.

<img src="/images/blog/2022-05-11-product-update.png" width="600" />

## Coming Soon - Payment Processing

What: Integrated payment processing functionality to enable community members to pay association dues, rent, and other common fees via credit/debit card and mobile pay services.

Why: It’s the digital age and community members expect to be able to make their payments without visiting an office, mailing a check, or making a phone call. Integrated payment processing allows residents to make their various payments online and on-the-go with the digital payment methods they prefer. This benefits communities by reducing administrative time, avoiding lost payments and creating additional trackability for who, when and what payments were made.
