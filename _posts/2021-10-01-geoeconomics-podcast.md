---
title: DoubleGDP Featured on Geoeconomics Podcast
categories:
    - company
author_staff_member: 2020-12_doreen
show_comments: false
image:
---

We're happy to have recently been featured on the Geoeconomics podcast. We talked about what a city operating system is and how it can help new cities connect with residents and accelerate their growth. Check it out on the [Geoeconomics website](https://www.adrianoplegroup.com/post/geoeconomics-podcast-ep-21-how-do-you-make-a-city-operating-system)
