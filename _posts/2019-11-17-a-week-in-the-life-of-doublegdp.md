---
title: A Week in the Life of DoubleGDP
categories:
  - company
author_staff_member: 01_jay
show_comments: false
---

I’ll admit out of the gate that in many ways this week was not typical.

*Two of us were in Zambia, with a customer, deploying new software.*

*Many of our interactions with our customer were in-person, instead of all-remote.*

*We spent as much time solving hardware issues and writing procedures as building software.*

*We operated under daily sprints instead of bi-weekly ones, across our product manager and front-end engineer on site in Zambia, our designer in San Jose and our lead engineer in Atlanta.*

And yet, I think that there’s no way better to get a feel for the culture of our team and what we’re trying to do together than by taking you through this week at DoubleGDP. It’s customer-centric. Results-oriented. Open and transparent by default. Quickly iterative.

&nbsp;  
&nbsp;  

## Wednesday

*Activities*
* Visit customers at the showroom, administrative offices and construction site.
* Conduct short 1-on-1 interviews with staff to review existing procedures for sales and construction site operations.

*Results*

On the customer side, they handed the keys over to the owners of the very first completed house on the site of their new town.

*Discovery*

The handoff between our customers’ sales and marketing systems may present an opportunity for support from our software that might re-orient some near term roadmap decisions.

&nbsp;  

## Thursday

*Activities*
* Train construction site operations lead on the mobile software that will shift the gate access control process his guards use from analog to digital.
* Develop process to print physical ID cards with QR codes to use with mobile phones at the gate.




*Results*

First two customer admins trained and accounts created on the new system.

*Discovery*

With a rapid change over of activity on the town’s site as first residents move in and construction scales up, new protocols at the gate aren’t yet fully developed to be translated into our software. Every software decision we make together with the customer will also be a process design decision for site operations.




*Iteration*

Shift roll-out and training of the guards to Friday so that the site operations lead can translate across language barriers and include the protocol decisions he made on Thursday.

&nbsp;  


## Friday

*Activities*
* Buy SIM cards and data so app can work on mobile phones at the construction site gate. Push code to fix a bug while waiting 2.5 hours to get that cellular data access.
* Train guards and construction site workers on the gate access app and new protocols.
* Conduct co-design session with the customers’ heads of marketing, sales, finance and operations to train them on the new app and identify future solutions that we can work on together.




*Results*

New active users logged in for 7 guards, 2 workers and 4 admin staff.




*Discovery*

The guards at the construction site were curious and immediately contributed questions and ideas for future development. They best understood the app as a digital version of their “entry log book.” Getting their logbooks accurately filled in by visitors at the gate is their primary process right now.




*Iteration*

Begin reinventing the guard view of the app to be a process flow, not a form. Design separate UX’s for logging information about individuals and construction trucks, which will have different process requirements for the guards going forward.


&nbsp;  

## Saturday

*Activities*
* Conduct three in-home ethnographic interviews with the first residents of our customers’ new town.
* Attend the weekend promotional picnic at the new town’s park and train the head of marketing to enroll new prospective residents in the app.

*Results*

Eight new users logged into the app by the customer, seven of which were new prospective residents to pass onto the sales team that wouldn’t have previously been captured without the app.




*Discovery*

Digital ID accounts and physical ID cards for visitors make prospective customers of the town feel welcomed and important. Phones on the construction site are often out of data making the existing log-in process via SMS code clunky with too many steps. We gained real empathy for the 14-hours a day of “load shedding” that reduce most Lusaka resident’s access to steady power, when our evening in-home interview was conducted nearly entirely in the dark.




*Iteration*

Build a 1-click SMS that the staff can use to create a new user and make the digital ID easier to open at the site in low data/low power situations.

&nbsp;  

## Sunday

*Activities*
* Return to picnic to launch and test the 1-click SMS feature.
* Conduct two more in-home ethnographic interviews for first-hand experiences of our customers’ customers to learn why they are moving to the new town.




*Results*

Visitor to the picnic site logged and first account created by staff using the 1-click feature.




*Discovery*

With construction groups coming through, users like existing clients or visiting prospectives are only a small portion of gate entries. The actual guards’ logbook is the best articulation of the process that – it’s believed - captures all entries today.




*Iteration*

Split enrolled users from non-enrolled entries that can be logged in a digital logbook by the guards. Design a digital logbook into the app.


&nbsp;  

## Monday

*Activities*
* Enroll the construction site operations lead in the updated protocol that we can support with the digital logbook that’s being designed.
* Acquire enough cellular data to run the gate app for the next month of activity.
* Conduct two in-home ethnographic interviews and begin debrief/analysis of findings.
* Conduct 1-on-1 interview with head of sales on today’s systems and future solutions.




*Results*

The new digital logbook is in process but not deployed from the dev environment and the site operations lead can’t get us out to re-train the guards on the new user flow.




*Discovery*

The visitors logged over the weekend present high value new leads that are hot and ready for a sales team follow-up, which is a great sign. There are too many screens to get through, though, for the sales team to find their phone numbers to make quick calls in-the-moment. Eventually, this exact process can be the link between the marketing and sales systems/processes.




*Iteration*

Begin designing quick-find solutions for the sales team of prospective clients’ phone numbers after they visit the town’s site. Approve the digital logbook in the dev environment and push to production.

&nbsp;  

## Tuesday

*Activities*
* Deploy new digital logbook flow at the gate and re-train the guards on the new feature.
* Conduct one in-home ethnographic interviews. Analyze findings about customers’ needs, mindsets and values around moving to a new town.
* Conduct 1-on-1 interview with head of marketing on today’s systems and future solutions.




*Results*

Ten entries logged by guards using the digital logbook, 100% of the entries through the gate after training.




*Discovery*

Instead of being intimidated by the technology and change, the guards were excited to learn something new and see this as a career advancement opportunity for them.




*Iteration*

Leave the front-end as is so the guards can get used to it. Make revisions on the back-end to increase stability and recover some of the week’s technical debt.


&nbsp;  


## Wednesday

*Activities*
* Meet with the staff for a “results and wins” presentation. Discuss next steps and what priorities they’re beginning to feel.
* Monitor user logs to make sure adoption by the guards at the gate is sticking.
* Host the customer for a celebratory dinner and get to know everyone personally.
* Get ready to depart Lusaka.




*Results*

Eight entries logged by guards using the digital logbook, 100% of the entries through the gate after training.




*Discovery*

We now have a live customer using our product as a critical part of their operations. It’s time to build out the customer support and customer success practices that will maintain uptime and increase adoption weekly. The target: weekly 10% active user growth. We also discovered that the light at the site at night made logging using the QR code more difficult.




*Iteration*

Determine approaches to launch a “torch” on the Samsung A10’s we’re using when it’s dark outside.



&nbsp;  
&nbsp;  




It was such a great week. Kudos to both our team for being open to a one-day-per-sprint rate of iteration while we were in Zambia, and all the folks on the customer side for their willingness to work alongside us sharing their needs and ideas.

We arrived in Lusaka with a new software solution for gate access control. We left with a completely re-designed solution, deployed and in use for every entry through the gate at the construction site of the new town. And just as importantly, our whole team learned more than we can wrap our heads around right now about our customer and the pioneering residents who will be the first ones choosing to move to their new town in the coming months.

Can’t wait to see what happens next week. So much more to come!
