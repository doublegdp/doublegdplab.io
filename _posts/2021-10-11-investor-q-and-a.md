---
title: Investor Q&A
categories:
    - company
author_staff_member: 2019-10_nolan
show_comments: false
image:
---

The following are answers we provided in conversation with potential investors (Y Combinator) in 2021-09. They wanted to see more sales traction before deciding whether to invest. However, the questions they asked and answers provided helped us to provide a fuller and more concise articulation of our strategy and market potential.

#### Do you have a demo available?

Yes, see here: [https://www.youtube.com/watch?v=ucCIBSxNUjc](https://www.youtube.com/watch?v=ucCIBSxNUjc)

#### Describe what your company does in 50 characters or less.

An end-to-end SaaS platform to run cities.  

#### What does your company going to make?

DoubleGDP is an end-to-end platform for cities to connect with their community and deliver efficiently-run services. It provides community members with a one-stop portal to access any services they need from the city; this will include paying for taxes, fees, utilities, registering businesses, applying for permits, reporting problems, asking for help. For the city administration it provides a foundational registry of the people, land, and entities in the city, in an integrated CRM and ERP with modern APIs to connect with a variety of third party software applications or hardware (e.g. meters, cameras, lights).

Today we focus on new cities in emerging markets; these are where municipal innovation will be fastest. The demand for better places to live, potential for rapid land value appreciation, and a few successful new cities will fuel a huge spike in new cities in the next decade.

#### Please share a 1-minute video introducing the founders.

[https://youtu.be/9S9oyxm46V8](https://youtu.be/9S9oyxm46V8)

#### Which category best applies to your company?

B2B SaaS

#### How far along are you?

We have 2 new cities using the platform and a pipeline of 10 opportunities with 3 in negotiation. Our two cities have a combined population of just 42 people, which generates a weekly product usage of ~200 active users, ~150 gate check-ins, and ~5 payments recorded. Various other active but sporadic usage includes recording time sheets of site workers, admin task management, form submission, promoting special events.

We’ve built an engineering team of 7 and customer success of 4. Given the global nature of our business, we’ve built this team all-remote and have team members in the US, 3 countries in Africa, and 2 countries in South America. We’ve established a culture and processes for effective asynchronous communication that allows us to recruit globally and onboard to a complex operational environment quickly.

#### How long have each of you been working on this? How much of that has been full-time?

I’ve been working full time on DoubleGDP for 2 years.

#### Are people using your product?

Yes

#### How many active users or customers do you have? If you have some particularly valuable customers, who are they?

Our two customers are Nkwashi (https://nkwashi.com) in Zambia and Ciudad Morazán (https://www.morazan.city/) in Honduras. Our ~200 weekly active users are of a variety of roles within the city administration: sales representatives to sell land, marketing to run promotions, guards to manage gate access, construction workers to log their time cards, and the finance team to record payments on plot leases. We have started to see some community engagement around messages and incident reports, though still early. I shared some more detailed activity metrics in the previous question as well.

#### Anything else you would like us to know regarding your revenue or growth rate?

We use population as our “north star” metric and aim to increase it ~20% month over month. We’ve grown from 10 to 42 residents this year and expect to be at 100 by end of year. Our goal next year is for 1000 residents across all DoubleGDP cities -- representative of expansion in our customer base and growth within each city.

We chose this metric with the YC philosophy of focusing on growth in mind, and with the strategy that our impact and all other metrics will increase in accordance with the total population of the cities we manage. We had started with 10% WAUs week over week, and grew to over 2000 WAUs, but switched to population when we saw that our metric was driving engagement but not value. We chose our current approach to reflect an ambitious target within the context of growing new cities and a timeline that reflects people moving to live in them.

#### Why did you pick this idea to work on? Do you have domain expertise in this area? How do you know people need what you're making?

I chose this idea because bringing city management to the 21st century has tremendous potential to improve our interaction with one of our most important relationships -- that with the city in which we live. Cities are economic engines of the world, and foundational determinants of the quality of life and access to opportunity for their residents. There’s huge demand to live in good cities, far outstripping supply and access. We expect in most aspects of our life to have a single portal through which to interact with a brand, and for that brand to have their act together enough to know who we are, what services we’ve used, and how to help us be successful. But with our cities we’ve come to expect little -- starting each interaction by filling out a form with our name and address and hopelessly slow bureaucracy.

At a more personal level, I enjoy solving hard operational problems that require systems thinking. I’ve loved living in San Francisco, though I've been frustrated at its frequent inept bureaucracy. I also have connections to both Africa and South America, the two primary markets for new cities. My uncle built his career in Africa; his stories and the trips we’ve all taken are now family lore. I spent nearly a year traveling around South America and learning Spanish last decade.

My domain expertise comes primarily from the last two years of working on this project. I’ve made two trips to Africa to meet new city developers in Zambia, South Africa, Kenya, and Nigeria. I’ve spoken with many more, including projects in Honduras, Paraguay, Malawi, and the US. I have prior background as a student in computer science, as a product manager building and delivering user-centric enterprise software, and as a CS leader deploying that software in complex operating environments.

New city entrepreneurs consistently express efficiency of services and technological enablement among their core value propositions. Their success depends on rapid land value appreciation, which depends on people deciding to move there en masse. A unified mobile app for their community and responsive services are obvious demonstrations of those values, but difficult to deliver in today’s landscape.


#### What's new about what you're making? What substitutes do people resort to because it doesn't exist yet (or they don't know about it)?

DoubleGDP is unique in providing an integrated approach to what most people would consider disparate city processes. New city entrepreneurs, like their mature city counterparts, today must either resort to building their own tech stack, integrating a variety of SaaS solutions, or contracting core IT functions to consultancy firms. The typical approach seems to be to allow each department to make their own decisions, precluding the kind of 360 customer (“community member”) view that in other industries grows customer lifetime value.

We also are taking a novel approach by focusing initially on new cities, itself a nascent market. New cities outside of China, of which we believe there are about 150 today, are mostly private sector companies that differ in key ways than their public sector existing city counterparts: they have an empowered executive leader with a long term time horizon, an imperative for growth, and an opportunity to start from scratch. (For them residents are customers, and customers expect more than voters.) Estonia today is heralded as one of the most efficient national governments because in 1991 they became independent of Soviet bureaucracy and restarted with an all-digital foundation. New cities can be even more impactful because, unlike in national innovations, thousands more new cities can follow in the footsteps of the early innovators, creating a startup community and multiplying the innovation and impact that can come from more efficient and effective administration.

Superficially it would seem we can better demonstrate product market fit by selling to existing cities. However, this may force us to narrow our focus to a single department or set of needs and to operate in political processes with many anti-growth incentives. Either of these would make it harder to realize the fully integrated approach and its huge value. We think it will be much better to approach an existing city after we have several formerly new city customers who have grown to a comparable size so we can showcase the value of running its administration via a modern platform.


#### Who are your competitors, and who might become competitors? Who do you fear most?

In the new city market our main competition is the city developer’s desire to build their own software, often with aims to scale it across multiple of their cities. We’ve seen some aspects of this approach with other potential prospects. Our first city customer tried this approach and adopted DoubleGDP once they experienced the first-hand difficulty of building this type of platform. We expect other cities to experience similar challenges to build such an ambitious platform when it’s out of their core competency and value proposition to do so.

As we grow to serve existing cities we see Tyler Tech (NYSE: TYL) as a competitor with a portfolio of SaaS offerings. They make some mention today of “integrated solutions” but their “browse products” page lists 50+ products each tailored to a specific departmental workflow; it’s a reasonable way to sell to those administrators but not a path to create a user-centric experience for residents. Many major tech and consulting companies sell IT to municipal governments. When DoubleGDP becomes the standard for new cities and transitions to serving existing cities, we’ll face significant entrenched interests (though also a wave of new service providers built around our open APIs to offer better experiences.)

What I fear most is the new city market not growing fast enough or to be substantial enough to attract investors, entrepreneurs and populace. Today, it’s dominated by visionaries or contrarians aiming to demonstrate a libertarian ideal or create a tax haven. This leads to impractical projects and makes the industry seem fringe. It obfuscates the opportunity to take undeveloped land, get people living on it, and reap spectacular returns while also improving a community’s quality of life. One new city builder shared that the value of his land in one city grew 30x in 6 years and in another 10x in 5 years. This is a small fraction of what he expects the appreciation to be in the next 5-10 years. This market should be able to attract the best and brightest around the world.

#### What do you understand about your business that other companies in it just don't get?

There’s significant similarities in core city administrative processes across countries and cultures. Most people tend to assume that each city is unique because of differences in its governing law or operating environment, but the core is fairly similar (and not that different from basic practices in the private sector). They need to market and sell properties, maintain a registry of land and businesses, publish and oversee workflows, manage access to the site, and connect various people and software systems to the data they need to get their respective tasks done.

It’s a challenging endeavor, but like GitLab does for DevOps and Workday does for ERP, DoubleGDP will provide a platform that supports strong core processes while allowing team-specific processes to be implemented and numerous other applications to connect via APIs.

#### How do or will you make money? How much could you make?
*(We realize you can't know precisely, but give your best estimate.)*

We have three revenue models today -- per capita, a share of city revenue, or a share of transactions facilitated by the platform. With transactions, we have signed a contract entitling us to a cut of transactions processed through the platform. (We will add this capability to the platform soon.) With the two other approaches we have agreed on price with a city but not yet closed a deal. One city agreed to pay us a share of their land sale revenue. Another pipeline opportunity has agreed to per resident per year fee.

The per capita approach is most straightforward to estimate our TAM. Govtech.com lists the US municipal IT market in 2021 is worth $58b. Assuming this extrapolates globally in proportion to GDP, it implies a global market of $193b, excluding China. Our end-to-end platform, once mature, should be able to capture between 20% and 50% of a city’s IT budget. Let’s assume we can capture 20% of the city market, the same as Salesforce and Workday hold in their respective markets. This would imply annual revenue between $7.7b and $19.3b, not factoring in macroeconomic trends that forecast growth in urbanization and government IT budgets.

Our learnings from our existing sales pipeline (albeit needing much more evidence) do align with this model. A global market of $193b and urban population of 3.5b (excluding China) implies a per capita expenditure of $55 today. Our price agreement at $10 per resident per year (18% of $55) while the product just scratches the surface of its eventual capabilities suggests that capturing 20% to 50% with a mature product is feasible.

Both the land sales and transaction models offer alternatives tied directly to city revenue, and potentially to the city’s GDP. According to the Urban Institute, local governments in the US collected $1.8t in revenue in 2018. Extrapolating worldwide by GDP, that’s $6t excluding China. Our arrangement ties us to the primary revenue stream of our city, which expects to sell over $1b of land in the next 10 years from their “phase 1” construction alone. A corresponding revenue-share arrangement with 20% of the city market would yield $15b in annual revenue. Transactions are less straightforward to estimate -- today it’s conceived of as transactions between the community and the city, but if we gain popularity as a platform to pay rent and utilities it’s easy to imagine expanding into other forms of mobile payments or community-based financing.


#### How will you get users? If your idea is the type that faces a chicken-and-egg problem in the sense that it won't be attractive to users till it has a lot of users (e.g. a marketplace, a dating site, an ad network), how will you overcome that?

In our early years, we will acquire users by selling to new cities and helping them grow. Our user numbers will scale with their population, since every resident will also attract some number of city administrators, workers, and visitors. Developing the market for new cities is thus central to our strategy.

Growing the population of new cities requires solving several coordination problems. People want to invest in land, build infrastructure, and move to a new home when they see others committed to do the same. Software can help, for example by promoting awareness of commitment, reducing the turnaround times between decisions and actions, even pooling capital or other resources. But it’s not sufficient. At Nkwashi, DoubleGDP started residency programs for artists and software developers in order to attract the initial residents and create a sense of life and community at what previously felt like a construction site. It’s an innovation that arose from our focus on growth and population as our primary KPI.

Those programs have driven some of our best insights about the real-world challenges facing new cities -- e.g. failure to get the water connected will delay move-ins -- and opened the doors with new prospects. The first question one property developer asked us in a sales call was about our “Hackers In Residence” and how our all-remote software developer training program was building the foundation of the city’s economy. They already had a vocational training program and wanted to expand their offerings leveraging our approach. We also have first hand experience managing the home-handover process, which is the first software workflow another prospective customer has asked us to implement.

#### If you had any other ideas you considered applying with, please list them. One may be something we've been waiting for. Often when we fund people it's to do something they list here and not in the main application.

The other idea that tempts me is to create an incubator for new cities. (Let’s call it “Y Cities”.) It would recruit a crop of entrepreneurs to start new cities, and equip them with the techniques, networks, and capital to succeed in exchange for an equity stake. New city founders today are portrayed as quixotic loners; a few big successes will make it seem possible the way Gates, Jobs, and Bezos paved the way for a rush of tech startups.

#### Please tell us something surprising or amusing that you have discovered.
*(The answer need not be related to your project.)*

Just before I graduated college, Malcolm Gladwell used me as the protagonist for a story about job interviews that was featured in the New Yorker and subsequently became part of the curriculum at several business schools. I’ve been amused by and appreciative of the life stories people have been inspired to share with me over the years when they first read it.

#### What convinced you to apply to Y Combinator? Did someone encourage you to apply?

I want to participate in YC to accelerate and broaden my learning. DoubleGDP faces challenges in several areas that are new to me and not covered in “startup 101”: navigating global cultures and markets, building an all-remote asynchronous and international team, municipal finance, and real estate development to name a few. Our product should incorporate simple approaches from many other software tools and get its value from having them all together. Being part of a stellar network like YC will help me quickly find people with experience in the aspect I need to solve and allow me to piggyback on readily available approaches where they work, focusing our innovation attention where it’s needed most. I also believe that I can add value to that network -- I gain energy and excitement from sharing what I’ve learned and the last couple of years have been a journey!

Sid encouraged me to apply.


### References

- Average spend of largest US cities per capita: [$2605 / year](https://ballotpedia.org/Analysis_of_spending_in_America%27s_largest_cities).
- Worldwide urban population: [56%](https://www.weforum.org/agenda/2020/11/global-continent-urban-population-urbanisation-percent/#:~:text=In%202020%2C%2056.2%20percent%20of,has%20risen%20in%20every%20content).
- US urban population: [249,253,271](https://www.census.gov/programs-surveys/geography/guidance/geo-areas/urban-rural/ua-facts.html)
- US local government IT expenditure: [$58b](https://www.govtech.com/navigator/data/2019-state-and-local-annual-it-spending.html)
- Workday hold of ERP market: [20%](https://www.techzine.eu/blogs/cloud/42772/workday-offers-a-solid-erp-system-and-sets-the-bar-high/#:~:text=According%20to%20him%2C%20Workday%20currently%20holds%2020%20percent%20of%20the%20market).
- Salesforce hold of CRM market: [20%](https://backlinko.com/salesforce-stats)
- Local governments revenue collected in US in 2018: [$1.8t](https://www.urban.org/policy-centers/cross-center-initiatives/state-and-local-finance-initiative/state-and-local-backgrounders/state-and-local-revenues)
