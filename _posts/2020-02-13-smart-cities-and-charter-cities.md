---
title: When You Look At Future Cities, What Do You See?
categories:
    - company
author_staff_member: 01_jay
show_comments: false
---
![Woven City master plan](/images/blog/smart-cities-and-charter-cities/Masterplan-für-Tangshan-Nanhu-Eco-City-wikimedia.jpg)

As a software company that’s entirely focused on new cities, our team at DoubleGDP is constantly talking with and reading about the visionaries, investors, developers, architects, planners and public servants who are bringing new communities to life.

From these discussions and musings, we are discovering that two different visions are emerging for future cities.  

&nbsp;  

# One Future: The Smart City

One vision of future cities took the stage at the recent 2020 Consumer Electronics Show (CES) - the **smart city**.

More than 400 exhibitors at CES shared their new smart city technologies with the world. Of all of them, this vision for the future of cities was most fully brought to life by Toyota’s exciting announcement of “Woven City.” It’s a prototype development where people, buildings, and vehicles are connected through sensors and data.

To quote [Business Insider](https://www.businessinsider.com.au/toyota-city-of-the-future-japan-mt-fuji-2020-1/amp)'s summary of their presentation,
> “The ‘city of the future’ will function as a testing ground for technologies like robotics, smart homes, and artificial intelligence and will be home to a starting population of 2,000 Toyota employees and their families, retired couples, retailers, and scientists, who will test and develop these technologies. Residents of the city, which Toyota has dubbed the “Woven City,” will live in smart homes with in-home robotics systems to assist with daily living and sensor-based artificial intelligence to monitor health and take care of other basic needs.”

Like much of what is shown at CES, Woven City’s vision for future cities is all about new-to-the-world technologies, engineering, prototyping, research and development. It’s fascinating, imaginative and filled with the potential of new ideas brought to life.

&nbsp;  

# Seeing a Different Future

A different vision of future cities will take the stage March 17-18 at the [Charter Cities Conference](https://chartercitiesconference.com/template.php?page=overview) (CCC) in Johannesburg. That conference will highlight another approach to the future of cities--what’s called a **charter city**.

Charter cities are new city developments that are granted special jurisdiction to create new governance systems. The purpose of the special jurisdiction is simple but powerful; it allows city officials to adopt the best practices in commercial regulation - like those that deal with trade, new business creation, taxation, infrastructure development, imigration, and voting rights, among many more.

These new charter cities aren’t just investing in new-to-the-world technology - they’re investing in a new way for how communities actually are created, built and function for their residents.

&nbsp;  

# Two Visions of the Same Concept

The more we at Double GDP looked at it, the more we’ve realized that the builders and backers of these two approaches to new cities use the same words to describe the parts and pieces of what they’re bringing to life: *mobility*, *connectivity*, *data-driven*. But these words hold very different meanings and focuses for each vision.

And for companies like ours that are building software to help new cities develop faster--and to attract new residents and businesses more quickly--the types of software solutions that we create need to be in-tune with the heart of what these cities are most trying to achieve.

As these two visions for the future came more into focus for us, we built the table below to help compare and contrast these visions for new smart cities and new charter cities:

&nbsp;  

| | New Smart Cities | New Charter Cities |
| --- | --- | --- | --- | --- | --- | --- | --- |
| *As Seen At...* <br /><br /> | *Consumer Electronics Show (CES)* in Las Vegas, NV USA | *Charter Cities Conference* in Johannesburg, South Africa <br /><br />|
| *The Big Picture* <br /><br /><br /><br /><br /><br /><br />| **Visions of Smart Cities** are...<br /><br />Tech-first <br /><br /> Focused on inventing new experiences <br /><br /> Often integrating new tech with existing infrastructure | **Visions of Charter Cites Cities** are...<br /><br />Community-first <br /><br />Focused on adopting best practices<br /><br /> Creating all physical and digital infrastructure, new, from day 1 <br /><br />|
| *Mobility* <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />| Mobility is about the **future of transportation**.<br /><br /> Autonomous driving, electric vehicles, charging stations, and new modes of transportation work together to remake how we navigate the cityscape and the impact we leave on the environment.<br /><br /> | Mobility is all about **accessibility** and **social movement**. It’s about people across the world being able to move to places where they can find the jobs, affordable homes and lives that they envision for themselves. <br /><br /> Property developers, technologists and governments work together to create communities with the resources, infrastructure and governance for new types of work to thrive. |
| *Connectivity* <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />| Connectivity is about **devices sharing data** with each other, businesses and city institutions.<br /><br /> 5G networks and the Internet of Things (IoT) make it possible for everything from street lights to water meters to provide insights and analytics about life in the city.<br /><br /> | Connectivity is about **people communicating** with each other, businesses and city institutions.<br /><br /> Widespread access to easy-to-use communication channels like SMS, chat and rapid feedback technologies help turn residents into citizens and make them feel like their voices are heard. <br /><br /><br /><br />|
| *Data* <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />| Data is collected and analyzed to make every interaction more **intelligent** and experience more **personalized.**<br /><br /><br /><br /><br /> <br /><br />| Data is collected and accessible to make public services more **efficient**, **transparent**, **fair**, **adaptable** and **responsive**.<br /><br /> In countries where corruption has been high and residents have been skeptical public officials, these new cities use a focus on data accessibility and transparency to improve public trust. And when the shared resources of cities are limited, better data leads to more efficient investments and faster adaptations. |
| *Startups and Entrepreneurs* <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />| Startups located in **today’s tech hubs** like Silicon Valley and Beijing invent new-to-the-world technologies as a way to change the fabric of urban life.<br /><br /> Innovators are excited about the potential for their technologies and see the smart cities of the future as where it all comes together. | Entrepreneurs and many large technology companies seek out **new locations in emerging markets like Africa** to locate their businesses and access **new talent pools**.<br /><br /> Businesses and entrepreneurs are looking for stable cities with good infrastructure to support jobs in fast growing industries like technology. <br /><br /><br /><br />|

&nbsp;  

We’re big believers in the future potential of both of these visions and will be supporting both over time.

Out of the gate, our first customers are building new communities that are more focused on building communities that are closer to the charter city vision. We’re building software to help them make that vision a reality. And we couldn’t be more compelled by the potential of these new cities to have a transformative effect on the lives and work of the people who come to call them home.

We’re excited to be joining these customers and others like them that the Charter Cities Institute is gathering for its inaugural conference in Joburg in late March. Our CEO, [Nolan Myers](https://twitter.com/nolanmyers?lang=en), will be speaking there about **Cities Powered by Software**.

Beyond our talk, we’re looking forward to two sessions on the upcoming CCC program that we think will help put various visions of future cities into perspective.

&nbsp;  

#### The Once and Future City (March 17)
“The desire to construct cities from scratch is as ancient as cities themselves. During this fireside chat, [Tamara Winter](https://twitter.com/_tamarawinter?lang=en), Communications Lead for the Charter Cities Institute and Daniel Brook, author of A History of Future Cities, will trace the history of planned cities, highlighting successes and failures along the way.”

&nbsp;  

#### Charter Cities in Context (March 18)
“The future of emerging economies will largely be determined in their cities. Gilbert Siame, Researcher in the Department of Geography and Environmental Studies at the University of Zambia, and Kilian Kleinschmidt, Founder and CEO of the Innovation and Planning Agency, Emmanuel Adegboye, Managing Partner of Utopia Lagos, join Wade Shepard, journalist and author of Ghost Cities of China, to discuss how charter cities can work within the context of rapid urbanization to help alleviate poverty, ensuring they're not exclusively havens for the wealthy.”

&nbsp;  

Please reach out if you’re going to be at the conference. We’d love to hear your vision for cities of the future, how you’re trying to bring that vision to life, and what we might contribute.

Email me at [**jay@doublegdp.com**](mailto:jay@doublegdp.com) and we’ll set some time up to talk.
