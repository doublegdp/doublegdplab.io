---
title: Coronavirus Memo
categories:
    - company
author_staff_member: 2019-10_nolan
show_comments: false
---
![Flatten The Curve](/images/blog/coronavirus/flatten-the-curve-6.jpg)

Note: this memo was shared with our Zambia team on 2020-03-15

After watching the news of coronavirus (COVID-19) in the last few days, it’s clear to me that it will be more impactful than just preventing my planned Africa trip this week. It certainly will have a big impact on the world for several weeks, and likely longer. Given this, it’s necessary for us to look at this challenge head-on both for how we stay safe today and how we help cities adjust to these types of issues moving forward. I wanted to share my perspective on where we are, what should be on our mind, and practical steps I’d like us to take right away.

This is a lengthy note, but please bear with me. I want to get us all on the same page, and with such limited face-to-face time I felt that written information will be most useful. Feel free to add comments or suggestions, and I’ll ensure we have some time to discuss next week. Also note that I’ve made this document publicly available; feel free to forward if you find it helpful.


## Three priorities

Here are the three priorities on my mind:



1. **Health and safety**. The most important is that we take care of ourselves and our loved ones. I have some recommendations below and encourage you to act quickly. Stores here began selling out of items once people got nervous and I hope that this note can help you get ahead of the curve.
2. **Business continuity**. Next is that we consider how to keep ourselves productive within a new context. We potentially have a vital role to play in the future of how our current customer and future cities handle events like these, but can only step into this if we continue to grow and improve.
3. **Help our customer** and consider how we might contribute to other communities’ safety now and in future. Public health and other emergencies tax the communications and resources of cities, and to be an end-to-end platform for connecting with residents and delivering services, we must have good solutions in place. There’s a lot we can learn from this experience, and we may also be able to contribute. (I don’t want to discuss this until you’ve had a chance to address #1 & 2, but have included it here so we have everything in one place.)

I’ll share more thoughts on how we should approach each of these below, but think it’s important first to ensure we operate with shared context of the situation.


## Context

I am learning and adjusting to new information that comes out, and though I’m not a doctor or public health professional I wanted to summarize my understanding of the situation.



1. Coronavirus (also known as COVID-19) is a novel strain of a known family of viruses (like influenza, the common cold, and more severe strains like SARS). It first appeared a few weeks ago in Wuhan, China and spread rapidly. It now has been observed in most countries around the world, leading the World Health Organization to deem it a “pandemic.”
2. Because it’s so new, there still a lot of uncertainty about its severity, treatment, appropriate public health response, etc. However, some things are reasonably well established now.
3. Some healthy people who contract it experience mild symptoms, and in many cases it looks much like a flu. Common symptoms are fever (often high), coughs, sneezes, headaches, body aches, sometimes (but less often than with flu) chills. In severe cases, it can cause lung and respiratory issues, including pneumonia.
4. For elderly people and those with underlying immune issues (or battling some other disease), COVID-19 often can be life threatening. The precise death rate of this virus is still unknown, but most estimates range from 10x to 30x as deadly as influenza.
5. We also know that it has a long incubation period -- it’s estimated to be 14 days from first exposure to when symptoms present. During some of this time -- estimates vary, but seem to be at least several days -- one can be transmitting it without having any symptoms.
6. Transmission takes place primarily through droplets from the respiratory system -- e.g. a cough, sneeze, or runny nose. But it appears that it can live on surfaces like door handles for quite a long time -- from hours to days.
7. The most likely way an individual can get it is by touching a person or surface with their hand and later touching their face prior to washing. For this reason, health professionals are recommending that you wash hands frequently, use 70%+ alcohol-based hand sanitizer when soap and water is not available, and stop using hand shakes as a greeting.
8. Governments are concerned about the capacity of their health-care systems and are mandating various “social distancing” techniques to “flatten the curve” of infections. We likely cannot stop this from spreading, but the plan is to slow it down sufficiently so that the health care system can meet demand for patients that need respirators or other intensive care.


![Flatten The Curve](/images/blog/coronavirus/flatten-the-curve-6.jpg)

## Health and Safety

The first priority in this is to take care of yourself and your family. Avoiding getting sick is not only important for your well being, but prevents you inadvertently spreading it to others.

The most valuable strategy for this is to **wash your hands frequently** and **avoid touching your face**. It may sound simple, but actually putting it into practice is quite challenging. Most people, myself included, touch their face unconsciously throughout the day. Each of those touches is a potential way that virus may get into your respiratory tract. Luckily, the structure of this virus is easily torn apart by soapy water. So **wash for 20 seconds at least once an hour** -- this simple act can go a long way toward improving your health and the health of those around you.

You also should have some sanitation supplies on hand at home. These quickly sold out within the US, so we can no longer buy them, but if they’re still available in Zambia I recommend you get some. Here’s a shopping list:



1. Hand sanitizer (e.g. Purell.) Get at least 70%+ alcohol content
2. Disinfecting wipes (e.g. Clorox)
3. Nitrile gloves
4. Rubbing alcohol and aloe gel (to make homemade hand sanitizer)
5. N-95 masks -- mostly useful to prevent unconscious face touching, but some still worry it may be transmissible through air
6. Also, hand soap (this hasn’t sold out here, thankfully.)

Second, I recommend you get some supplies at home. This can be useful in case grocery stores and restaurants shut down. They may be more difficult to buy if the virus spreads in Zambia and people begin to worry. Here’s some items I recommend:



1. Flu medicine
2. Toilet paper
3. Canned or dried food -- ~2 week supply in case of forced quarantine
4. Vodka or gin. To make “[quarantinis](https://thenovicechefblog.com/quarantini/)” at home. (Just kidding)

Finally, be cautious not to spread virus:



1. Wash hands for 20 seconds each time you arrive home, every hour during day
2. Use hand sanitizer if soap and water not available
3. Cough into your elbow rather than your hand
4. Use elbow bumps instead of hand shakes
5. Keep 3 ft distance from others
6. Stay at home if feeling at all ill
7. [https://www.google.com/search?q=coronavirus+tips&fbx=dothefive](https://www.google.com/search?q=coronavirus+tips&fbx=dothefive)


## Business continuity

Assuming we’re healthy, we want to remain productive. Here are some steps we should take:



1. If still available in Zambia, buy some hand sanitizer. It would be great to get [automatic dispensers](https://www.amazon.com/Advanced-Sanitizer-Starter-Touch-Free-Dispenser/dp/B005UXPBBK) and several refills, but I think anything we can get will be helpful. I’d aim provide for four locations: main gate entrance, your office, the sales showroom, and one spare.
2. If still available, buy nitrile gloves for CSMs and guards to wear at gate
3. Set up a home office. Now’s the time to ensure you’re able to work from home if need be. DoubleGDP will reimburse reasonable expenses. Please ensure at least these items:
    1. Good data plan
    2. Battery pack for power storage
    3. Also consider items from this list: [https://about.gitlab.com/handbook/spending-company-money/#office-equipment-and-supplies](https://about.gitlab.com/handbook/spending-company-money/#office-equipment-and-supplies).

Note that if you need to get several of these, please make a list and let’s discuss in advance. The GitLab prices are not relevant in Zambia and we are not in a position yet to be as generous with expenses as they are. However, I do want to ensure you can work productively from home, so please let me know what will help make that possible.


## Help others

Cities have an important role to play in public health and in emergency management. We should aim to help our customers wherever possible, and also consider ways we may help other communities either now or longer-term.

For our customers, here are some ideas we’ve discussed:



1. Virtual show-room -- can we bring some of their assets online?
2. Video sales calls -- can we help their sales team work remotely?
3. Resident / client communications -- when and why might they need to message with clients and are there better methods than what they have set up today?
    1. Now they use email and SMS blast. Can get read receipts for email, but SMS doesn’t provide feedback
    2. What kind of communications would residents / clients want in this situation?
4. Digital contract management workflow -- can the process of enrolling a client be managed remotely, with the necessary contracts and paperwork?

For others, some ideas on my mind:



1. What kinds of communications do cities need in this situation?
2. What does “responsive public services” look like in this (or other emergency situations) and how does a city organize around it?
    1. Ensuring healthcare capacity
    2. Distributing necessary supplies
    3. Engaging first responders and arming with information
    4. Surging capacity -- recruiting, training, and deploying staff for new situations
3. Should we do some content marketing around this?
4. Can we make a simple communication product available to other communities immediately?
    5. Every community large and small will want to be addressing this. What features could we offer that would help?

<!-- Docs to Markdown version 1.0β19 -->
