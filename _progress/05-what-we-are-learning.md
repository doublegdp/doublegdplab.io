---
title: What We're Learning
name: What We're Learning
subtitle: Notes and articles we'd like to learn from
external_url: https://airtable.com/shrsfwF5j5XfZT8fq/tbl0PUn3rAZDOJnK4?backgroundColor=orange&viewControls=on&blocks=hide
image_path: /images/progress/WhatWeAreLearningAirtable3.png
parent: progress.html
---

Here, we're publishing a view of our knowledge database so that we can share what we're learning with the broader community.

This helps us live out a few key values.

As much as possible, we're public by default. Transparency drives trust.

We want to build a single source of truth for everyone - leaders, company, customers and public. It's more efficient and more transparent, too.

And it allows us to work as part of the community. This adds diversity to our thinking, and let's everyone contribute to improving this product.

At the link below, there's a form where anyone can contribute articles, reports and ideas you think the community should see. The form adds an entry to this knowledge database, and our who team will see it on Slack.

If you'd like to contribute, please do. Anyone can contribute. Please add both the "what" (the facts) and the "why" (the insight and why you think it matters).

<iframe class="airtable-embed" src="https://airtable.com/embed/shrsfwF5j5XfZT8fq?backgroundColor=orange&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>

[Click here to add an insight about new software for new cities](https://airtable.com/shrZGPMbZRH4DjTML)
