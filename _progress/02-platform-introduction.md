---
title: Platform
name: DoubleGDP Platform
subtitle: An End-to-End Platform for New Cities
external_url: https://docs.google.com/presentation/d/13Eszn7sHJ7Difi5ZRVqDL4RBFjTMQkIBX1QP3qdKDVQ/edit?usp=sharing
image_path: /images/platform-features.png
parent: approach.html
---

At DoubleGDP, we're building an end-to-end platform for new cities to connect with residents, accelerate growth and deliver responsive public services.

Our platform makes municipal and community services more efficient, accountable, adaptable and responsive.

It makes frequently used services processes more easily **repeatable** - so they’re far more efficient and more equitable.

It makes the results of services **measurable** - so they’re accountable to residents’ expectations.

It offers easy-to-use communication tools so residents and businesses can provide **feedback** and feel heard.

## DoubleGDP Platform

1. Mobile-First Web Portal
2. Resident Engagement Platform
3. Digital Marketing Platform
4. Sales & Support Platform
5. City Management Platform

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSZLsZIiTZ0_P4d1OubuuItAOPeRJWtNWfbEe9PCkFMk_5gfYnh_Ys3Vj8k3RDLiLGYmClkW0SOR64C/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
