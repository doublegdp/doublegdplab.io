---
title: Culture Handbook
name: Culture Handbook
subtitle: A central repository of our culture, values and work practices
external_url: https://handbook.doublegdp.com
image_path: /images/progress/DoubleGDPvalues.png
parent: progress.html
---


## Results
We do what we promised to each other, customers, users and investors.

* Measure results not hours.
* Give agency and take ownership.
* Write promises down.
* Bias for action.
* Strive for ambitious results while iterating with small changes.


## Transparency
Be open about as many things as possible. By making information public we can increase the likelihood of trust, reduce the threshold to contribution and make collaboration easier.

* Be public by default.
* Be direct in your communication.
* Say why, not just what.
* Build a single source of truth for everyone - leaders, company, customers and public.
* Disagree, commit, and disagree. Everyone can be questioned, any decision can be changed.


## Efficiency
We care about working on the right things, not doing more than needed, and not duplicating work. This enables us to achieve more progress, which makes our work more fulfilling.

* Create boring solutions.
* Ship the minimum viable change.
* Write things down.
* Set clear objectives and give freedom to work on them.
* Seek efficiency for the right group.


## Iteration
We do the smallest thing possible and get it out as quickly as possible.

* Don't wait.
* Reduce cycle time.
* Work as part of the community.
* Do things that don't scale.
* Focus on improvement.


## Diversity
Foster an environment where everyone can thrive - inside our companies and in the communites we support.

* Seek values fit not cultural fit.
* Build safe communities.
* See something, say something.
* Don't bring religion or politics to work.
* Put family and friends first, work second.


## Collaboration
Helping others is a priority. You can rely on others for help and advice. The person who's responsible decides how to do it, but take each suggestion seriously and be responsive.

* Design for the stakeholders.
* Demonstrate kindness.
* Assume positive intent.
* Have short toes.
* Anyone can chime in on any subject.
