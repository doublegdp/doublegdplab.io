---
title: Sprints
name: Sprint Updates
subtitle: Video recaps of bi-weekly sprints
external_url: https://www.youtube.com/channel/UCALY7l5iisNVrEyvLgQa3ig/playlists
image_path: /images/progress/youtubechannel_photo.png
parent: progress.html
---

We work in the open as much as we possibly can.

To practice our values of transparency, iteration and collaboration, we record and post our bi-weekly sprint udpates on YouTube.

These are casual conversations amongst our team about progress we're making and what we're learning.

We typically don't edit the videos unless it's important to remove some personally identifying information that came up in the discussion.

We hope that if we create a company and product that puts transparency, iteration and efficiency at the core of its activities, we're more likely to build a platform that enables those same types of benefits for the residents and employees of the cities that use it.


## Typical Discussion Topics

* Product updates and sprint results
* Customer updates
* Company & recruiting updates


## Other Videos on DoubleGDP's YouTube Channel

* Product Demos
* Leadership Interviews
* Customer Interviews
