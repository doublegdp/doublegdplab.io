---
title: Product Development
name: Product Development
subtitle: How we approach product development
external_url: https://drive.google.com/drive/u/1/folders/1oHUmTsiw3GV_vrILXbuMKLDWBEY-0U-o
image_path: /images/progress/product_sketch.png
parent: progress.html
---

We aim to have a minimum viable product running in Nkwashi by end of the year. It will cover gate access, resident communications, and payment histories. Below are the artifacts we are using to design it:

* [MVP Goals and Use Cases](https://docs.google.com/document/d/1UT6-8InZZ6uCBG8RWtbROxid21c3mt-8INweP1eDhQI/edit)
* [Product Sketches and Descriptions](https://docs.google.com/document/d/1O7_hlMlE5RnsmLBIXq19rm1z0pw1wXI9DGbi0agUpcs/edit#)
* [Personas](https://docs.google.com/presentation/d/16IUZTk05C3Khszx-lwLTbYvL3s_kvB6sjy0DkilSA_8/edit#slide=id.g6122d8842c_0_5)
* [Discovery Document](https://paper.dropbox.com/doc/Nkwashi-Discovery--AkrK6SNwXpb~wVjztiWv~SewAg-ZgzMmJmpk19vAPFIX6XRC)
* [Workflow Diagram](https://drive.google.com/open?id=1LgDY0o9BF4ruSV87emypy_kkYUMh6Tw7)
* [Training Material](https://docs.google.com/presentation/d/1e5-k8fiervycACPyG2tmQxXEumcZr3cEt_FsXhBf57E/edit?usp=sharing)
