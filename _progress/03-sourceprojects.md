---
title: Source Available Projects
name: Source Available Projects
subtitle: Code repository for our projects
external_url: https://gitlab.com/doublegdp
image_path: /images/progress/gitlab-blog-cover.png
parent: progress.html
---

Transparency drives trust.

There's nothing more important to residents of a city than trust.

Everyone can contribute to and inspect our projects. We host our code repositories on GitLab.


## Current Projects

* DoubleGDP Website
* Mobile Access Control MVP for Zambia Construction Site
