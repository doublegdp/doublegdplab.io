---
title: Our approach to data
name: Data Approach
subtitle: We aim to be transparent and respectful in our use of data
parent: approach.html
image_path: /images/approach/data.jpg
---

Where people live affects growth. We help the world build great places to live. Our aim is to provide an end-to-end platform for builders of planned communities in order to help those communities grow rapidly and run efficiently, responsively and free of corruption — to create places where people want to live, work and play. We use software and data analytics to reduce inefficiencies, improve reliability and increase accountability in community services, which means managing a variety of data that is important to our customers, their clients, and our business. We are committed to providing transparency about what we collect, how it is used, and what we do to keep sensitive information safe.

* DoubleGDP is delivered as Software as a Service (SaaS), like other modern systems such as Salesforce and Workday. SaaS systems are managed by the vendor and multi-tenant, making them much less expensive to improve and maintain than self-hosted services. Using SaaS services over the cloud means not having to maintain infrastructure or manage software upgrades. SaaS makes it easy for you to remain current with regulatory updates to software and be at the forefront of technology due to more frequent product releases.
* We allow customers to synchronize, export, and delete the data associated with their community, except when restricted by legal obligations or user expectations. Examples of legal obligations may include discovery of evidence that prevents deletion or compliance with applicable privacy laws. Examples of user expectations may be to ensure that the results of anonymous surveys stay anonymous and that complaints are visible only to the extent required to address them.
* Customers retain ownership of the data they house in the DoubleGDP platform, such as client or resident lists. They grant us a non-exclusive right to use it to make the site function, with the understanding that we may not sell or use individual's data for marketing or other purposes.
* Part of our mission is to help planned communities learn from one another, and to this end we do expect the ability to generate and share aggregated usage statistics from customer data. Such statistics do not contain any personally identifiable information (PII). 
* We also recognize that certain rights must be retained by clients and residents of our customers. For example, the community retains copyrights to posts or communications made through the platform; but like other social media platforms, DoubleGDP may distribute it and allow others to redistribute it as well. 


<!--
Today, we collect three types of information:
1. *Identification data* such as names, phone numbers, emails, national registration card numbers, and vehicle license plates. This data is used to allow us to uniquely identify people and provide our customers with appropriate information in order to decide whether to allow a visitor into the site.
1. *Gate access logs* -- we maintain a log of all visitors to our customers’ community sites, what security guard was on duty, and who granted access to the site.
1. *Application usage* -- we track a variety of behaviors on our website ([https://doublegdp.com](https://doublegdp.com)) and within our application ([https://app.doublegdp.com](https://app.doublegdp.com)) in order to understand its usage and diagnose issues if they occur. We use Google Analytics to log activity.

We have several levels of control over who has access to the information:
* Community Administrators have access to all community data, and have the ability to create new users and grant site access.
* Community Security Guards have access to current gate logs while logged into the application
* Community Clients (or any other members) have access to their personal data only
* DoubleGDP Administrators (fewer than 10 people) have access to both app usage data and community data
-->
We recognize the importance of data security and we regularly review and evaluate our approach to managing this information as the number of users and use cases grow, and needs for detail and specificity increase.
