---
title: Overview
name: DoubleGDP Overview
subtitle: Vision for the software platform and company
external_url: https://docs.google.com/presentation/d/1c-Ci9QVSc-H0vwtelS0mL4Ka5v2kbzr-1BfOSfLp4d4/edit?usp=sharing
image_path: /images/progress/DoubleGDP_Overview.png
parent: approach.html
---

As with everything at DoubleGDP, we're openly sharing the overview, strategy and roadmap for this business.

We believe that the more people who know what we're trying to do, the better.

We'll attract the right community of customers, teammates and partners to work alongside.

And if another company makes great progress on this challenge based in some small part on what they learn from us, we'll all be the better for it.


## DoubleGDP Overview

* Vision
* Unmet Customer Needs
* Market Overview
* Solution
* Platform Roadmap
* Traction
* Business Model
* Competition
* Leadership Team
* Timeline
* Asks

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRfBwiws11-FRL_ogTR5S29F909vb1-uqft2K062EkiaaLTeLueEGJHHt3bodQED1QT9MNEd2ojoSwF/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
